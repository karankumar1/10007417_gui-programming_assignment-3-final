function changefont(){
    if (text12.checked){
        document.body.style.cssText+=';font-size: 12px;';
        
    }else if (text14.checked) {
        document.body.style.cssText+=';font-size: 14px;';
    }
    else if (text18.checked) {
        document.body.style.cssText+=';font-size: 18px;';

    }
    else if (text24.checked) {
        document.body.style.cssText+=';font-size: 24px;';

    }
}

function wallpaper(){
    if (wallpaper1.checked){
        document.body.style.cssText+=';background-image: url(img/wood.png);background-repeat: no-repeat;';
        
    }else if (wallpaper2.checked) {
        document.body.style.cssText+=';background-image: url(img/snow.jpg);background-repeat: no-repeat;';
    }
    else if (wallpaper3.checked) {
        document.body.style.cssText+=';background-image: url(img/sea.jpg);background-repeat: no-repeat;';

    }
}

function font() {
    if (lucidafont.checked){
        document.body.style.cssText+=';font-family: Lucida Console;';
        
    }else if (cambriafont.checked) {
        document.body.style.cssText+=';font-family: Cambria;';
    }
    else if (georgiafont.checked) {
        document.body.style.cssText+=';font-family: Georgia;';

    }
}